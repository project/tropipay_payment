<?php

namespace Drupal\tropipay_payment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides the Tropipay Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "tropipay_redirect_checkout",
 *   label = @Translation("Tropipay (Redirect to quickpay)"),
 *   display_label = @Translation("Tropipay"),
 *    forms = {
 *     "offsite-payment" = "Drupal\tropipay_payment\PluginForm\OffsiteRedirect\TropipayPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 *  requires_billing_information = TRUE,
 * )
 */
class TropipayPayment extends OffsitePaymentGatewayBase implements HasPaymentInstructionsInterface {
  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The locking layer instance.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   *   The language manager.
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentRouteMatch = $container->get('current_route_match');
    $instance->lock = $container->get('lock');
    $instance->logger = $container->get('logger.factory')->get('tropipay_payment');
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  public function defaultConfiguration() {
    return [
        'mail' => '',
        'passw' => '',
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail'),
      '#description' => $this->t('This is the mail for the account in Tropipay.'),
      '#default_value' => $configuration['mail'],
      '#required' => TRUE,
    ];

    $form['passw'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The passwor for the account in Tropipay.'),
      '#default_value' => $configuration['passw'],
      '#required' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['mail'] = $values['mail'];
    $this->configuration['passw'] = $values['passw'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $configuration = $this->getConfiguration();

    $instructions = [];
    if (!empty($configuration['instructions']['value'])) {
      $instructions = [
        '#type' => 'processed_text',
        '#text' => $configuration['instructions']['value'],
        '#format' => $configuration['instructions']['format'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // @todo Add examples of request validation.
    // Note: Since requires_billing_information is FALSE, the order is
    // not guaranteed to have a billing profile. Confirm that
    // $order->getBillingProfile() is not NULL before trying to use it.
    /*$payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $request->query->get('txn_id'),
      'remote_state' => $request->query->get('payment_status'),
    ]);
    $payment->save();*/

    // Do not process the notification if the payment is being processed.
    /* @see \Drupal\commerce_sermepa\Plugin\Commerce\PaymentGateway\Sermepa::onNotify() */
    if ($this->lock->lockMayBeAvailable($this->getLockName($order))) {
      $this->processRequest($request, $order);
    }
    else {
      // Wait for onNotify request that is doing this work, this occurs on
      // asynchronous calls, when  onNotify and onReturn can collide.
      $this->lock->wait($this->getLockName($order));
    }

    // We could have an outdated order, just reload it and check the states.
    // @TODO Change this when #3043180 is fixed.
    /* @see https://www.drupal.org/project/commerce/issues/3043180 */
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $updated_order = $order_storage->loadUnchanged($order->id());

    // If we have different states is because the payment has been validated
    // on the onNotify method and we need to force the redirection to the next
    // step or it the order will be placed twice.
    if ($updated_order->getState()->getId() != $order->getState()->getId()) {
      // Get the current checkout step and calculate the next step.
      $step_id = $this->currentRouteMatch->getParameter('step');
      /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
      $checkout_flow = $order->get('checkout_flow')->first()->get('entity')->getTarget()->getValue();
      $checkout_flow_plugin = $checkout_flow->getPlugin();
      $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);

      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $updated_order->id(),
        'step' => $redirect_step_id,
      ])->toString());
    }

    $this->messenger()->addStatus($this->t('Your payment has been completed successfully.'));

  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    try {
      // At this point we can not check if the order is locked, we do not have
      // the order, we just continue and check if it is locked when we have the
      // order.
      $this->processRequest($request);
    }
    catch (\Exception $exception) {
      // Nothing to do. ::processRequest throws exceptions if the payment can
      // not be processed, and returns an error 500 to Sermepa/Redsýs.
    }
  }


  /**
   * Processes the notification request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the payment is valid, otherwise FALSE.
   *
   * @throws \CommerceRedsys\Payment\SermepaException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function processRequest(Request $request, OrderInterface $order = NULL) {
    // Capture received data values.
    /*$feedback = [
      'Ds_SignatureVersion' => $request->get('Ds_SignatureVersion'),
      'Ds_MerchantParameters' => $request->get('Ds_MerchantParameters'),
      'Ds_Signature' => $request->get('Ds_Signature'),
    ];

    if (empty($feedback['Ds_SignatureVersion']) || empty($feedback['Ds_MerchantParameters']) || empty($feedback['Ds_Signature'])) {
      throw new PaymentGatewayException('Bad feedback response, missing feedback parameter.');
    }*/
    //$strrrr=file_get_contents('drupal://input');
    $strrrr=$request->getContent();
    $ppd=json_decode($strrrr,true);

    $this->logger->notice("JSON string: " . $strrrr);
    // Get the payment method settings.
    $payment_method_settings = $this->getConfiguration();

    // Get order number from feedback data and compare it with the order object
    // argument or loaded.
    //$parameters = $gateway->decodeMerchantParameters($feedback['Ds_MerchantParameters']);
    //$order_id = $parameters['Ds_MerchantData'];
    $ds_amount = $ppd["data"]["originalCurrencyAmount"];
    $ds_order = $ppd["data"]["reference"];
    $ds_bankordercode = $ppd["data"]["bankOrderCode"];
    //$ds_amount = $ppd["data"]["originalCurrencyAmount"];
    $ds_merchant_usermail = $payment_method_settings['mail'];
    $ds_merchant_userpassword = $payment_method_settings['passw'];
    $ds_reference=$ppd["data"]["reference"];
    $ds_currency = $ppd["data"]["currency"];
    //intval(substr($ds_reference,0,-4));
    $order_id = intval(substr($ds_reference,0,-4));

    $firma_remota = $ppd["data"]["signature"];

    $firma_local=hash('sha256', $ds_bankordercode . $ds_merchant_usermail . sha1($ds_merchant_userpassword) . $ds_amount);

    if ($order === NULL) {
      $order_storage = $this->entityTypeManager->getStorage('commerce_order');
      $order = $order_storage->load($order_id);
    }
    if ($order === NULL || $order->id() != $order_id) {
      $this->logger->warning('The received order ID and the argument order ID does not match.');
    }

    // The onNotify and onReturn methods can collide causing a race condition.
    if ($this->lock->acquire($this->getLockName($order))) {
      // Validate feedback values.
      if ($firma_local!=$firma_remota) {
        $this->lock->release($this->getLockName($order));
        throw new PaymentGatewayException('Bad feedback response, signatures does not match.');
      }

      if ($ppd["status"]=="OK") {
        /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

        // Check if the payment has been processed, we could have multiple
        // payments.
        $payments = $payment_storage->getQuery()
          ->condition('payment_gateway', $this->parentEntity->id())
          ->condition('order_id', $order->id())
          ->condition('remote_id', $ds_bankordercode)
          ->execute();

        if (empty($payments)) {
          /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
          $payment = $payment_storage->create([
            'state' => 'completed',
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $this->parentEntity->id(),
            'order_id' => $order->id(),
            'test' => $this->getMode() == 'test',
            'remote_id' => $ds_bankordercode,
            'remote_state' => 'OK',
            'authorized' => $this->time->getRequestTime(),
          ]);
          /*$status_mapping = $this->getStatusMapping();

          if (isset($status_mapping[$this->getConfiguration()['transaction_type']])) {
            $payment->setState($status_mapping[$this->getConfiguration()['transaction_type']]);
          }*/

          if (!$order->get('payment_method')->isEmpty()) {
            /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $credit_card */
            $credit_card = $order->get('payment_method')
              ->first()
              ->get('entity')
              ->getTarget()
              ->getValue();
            $payment->set('payment_method', $credit_card)->save();
          }
          $payment->save();
        }

        $this->lock->release($this->getLockName($order));

        return TRUE;
      }

      $this->lock->release($this->getLockName($order));

      throw new PaymentGatewayException('Failed attempt, the payment could not be made.');
    }
  }

  /**
   * Returns the lock name.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string
   *   The built lock name.
   */
  protected function getLockName(OrderInterface $order) {
    return 'tropipay_payment_process_request_' . $order->uuid();
  }

}


