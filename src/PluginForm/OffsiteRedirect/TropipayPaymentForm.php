<?php

namespace Drupal\tropipay_payment\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class TropipayPaymentForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Payment gateway configuration data.
    $data['version'] = 'v10';
    $data['merchant_id'] = $configuration['merchant_id'];
    $data['agreement_id'] = $configuration['agreement_id'];
    $data['language'] = $configuration['language'];

    // Payment data.
    $data['currency'] = $payment->getAmount()->getCurrencyCode();
    $data['total'] = $payment->getAmount()->multiply(100)->getNumber();
    $data['variables[payment_gateway]'] = $payment->getPaymentGatewayId();
    $data['variables[order]'] = $payment->getOrderId();

    // Order and billing address.
    $order = $payment->getOrder();
    //$billing_address = $order->getBillingProfile()->get('address');
    $billing_address = $order->getBillingProfile()->address->first();
    $data['name'] = $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName();
    $data['address']=$billing_address->getAddressLine1();
    $data['city'] = $billing_address->getLocality();
    $data['postalcode']=$billing_address->getPostalCode();
    $data['state'] = $billing_address->getAdministrativeArea();

    //------aquí conecto con la api----------
    // $payment_gateway_plugin->getMode();  //Live or Test
    //$tropipay_server=tropipay_payments_server_url($settings['server_tropipay']);
    if($payment_gateway_plugin->getMode()=="live") {
      $tropipay_server="https://www.tropipay.com";
    }
    if($payment_gateway_plugin->getMode()=="test") {
      $tropipay_server="https://tropipay-dev.herokuapp.com";
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $tropipay_server . "/api/access/login",
      //CURLOPT_HTTPHEADER => array ('Content-Type: application/json','Content-Length: ' . strlen($data_string)),
      CURLOPT_HTTPHEADER => array ('Content-Type: application/json'),
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\"email\":\"" . $configuration['mail'] ."\",\"password\":\"" . $configuration['passw'] . "\"}",
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;
      $character = json_decode($response);
      $tokent=$character->token;

      $datetime = new \DateTime('now');
      //echo $datetime->format('Y-m-d');

      //$customerprofiled=commerce_customer_profile_load($order->commerce_customer_billing['und'][0]['profile_id']);

      $arraycliente["name"]=$billing_address->getGivenName();
      $arraycliente["lastName"]=$billing_address->getFamilyName();
      $arraycliente["address"]=$data['address'] . ", " . $data['city'] . ", " . $data['postalcode'];
      $arraycliente["phone"]="+34616161611";
      $arraycliente["email"]=$order->getEmail();
      $arraycliente["countryId"] = 1;
      $arraycliente["termsAndConditions"] = true;

      $datos=array(
        "reference" => str_pad($order->id(), 8, "0", STR_PAD_LEFT).date('is'),
        "concept" => t('Order #: ') . $order->id(),
        "description" => " ",
        "amount" => $data['total'],
        "currency" => $data['currency'],
        "singleUse" => true,
        "reasonId" => 4,
        "expirationDays" => 1,
        "lang" => "es",
        "urlSuccess" => $form['#return_url'],
        "urlFailed" => $form['#cancel_url'],
        "urlNotification" => $payment_gateway_plugin->getNotifyUrl()->toString(),
        "serviceDate" => $datetime->format('Y-m-d'),
        "directPayment" => true,
        "client" => $arraycliente
      );

      $data_string2 = json_encode($datos);

      //dsm($data_string2);
      \Drupal::logger('tropipay_payment')->notice($data_string2);

      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $tropipay_server . "/api/paymentcards",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data_string2,
        CURLOPT_HTTPHEADER => array(
          "authorization: Bearer " . $tokent,
          "content-type: application/json"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        //echo $response;
        $character = json_decode($response);
        $shorturl=$character->shortUrl;


        //$form['#action'] = $shorturl;
        //dsm($form);
        /*$form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Proceed to Tropipay'),
          );*/
        return $this->buildRedirectForm(
            $form,
            $form_state,
            $shorturl,
            array(),
            self::REDIRECT_GET
          );
      }
    }




    //------------------------

    // Form url values.
    /*$data['continueurl'] = $form['#return_url'];
    $data['cancelurl'] = $form['#cancel_url'];

     return $this->buildRedirectForm(
      $form,
      $form_state,
      'https://payment.my_payment_provider.net',
      $data,
      self::REDIRECT_GET
    );*/
  }

}
